package nugroho.dimas.uts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import kotlinx.android.synthetic.main.fragsetting.view.*

class fragmensetting : Fragment(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.editKat->{
                fr = activity!!.supportFragmentManager.beginTransaction()
                fr.replace(R.id.FrameLayout,fragsubsetting).commit()
                fr.addToBackStack(null)
            }
        }
    }

    lateinit var v : View
    lateinit var fr : FragmentTransaction
    lateinit var thisParent : fraginti
    lateinit var fragsubsetting : fragsubsetting

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        thisParent = activity as fraginti
        v = inflater.inflate(R.layout.fragsetting,container,false)
        v.editKat.setOnClickListener(this)
        fragsubsetting = fragsubsetting()

        return  v

    }
}