package nugroho.dimas.uts

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.frgsubsetkingkategori.*
import kotlinx.android.synthetic.main.frgsubsetkingkategori.view.*

class fragsubsetting : Fragment(), View.OnClickListener , AdapterView.OnItemSelectedListener {

    lateinit var v: View
    lateinit var thisparent: fraginti
    lateinit var listadap: ListAdapter
    lateinit var spadapjen: SimpleCursorAdapter
    lateinit var db: SQLiteDatabase
    lateinit var buider: AlertDialog.Builder
    var nmjen: String = ""
    var id_jen : String =""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.frgsubsetkingkategori, container, false)
        thisparent = activity as fraginti
        db = thisparent.getdatUang()
        buider = AlertDialog.Builder(thisparent)
        v.spinnerjen.onItemSelectedListener = this
        v.insert.setOnClickListener(this)
        v.edite.setOnClickListener(this)
        v.del.setOnClickListener(this)
        v.lsNIl.setOnItemClickListener(clik)

        return v
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.insert->{
                buider.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data Kategori yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", inbt)
                    .setNegativeButton("Tidak", null)
                buider.show()
            }
            R.id.del->{
                buider.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah anda Yakin Menghapus Data ini !!!")
                    .setPositiveButton("Ya", indel)
                    .setNegativeButton("Tidak", null)
                buider.show()

            }
            R.id.edite->{
                buider.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah Anda mengupdate Data ini !!!")
                    .setPositiveButton("Ya", upbt)
                    .setNegativeButton("Tidak", null)
                buider.show()

            }

        }
    }

    override fun onStart() {
        super.onStart()
       // showjenis()
        showkat()
        showjenisAll()
    }

    fun showjenisAll() {
        var sql = ""

        sql = "select m.id_jenis as _id , m.nama_jenis , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori"

        val c: Cursor = db.rawQuery(sql, null)
        listadap = SimpleCursorAdapter(
            thisparent,
            R.layout.isikategori,
            c,
            arrayOf("_id", "nama_jenis", "nama_kategori"),
            intArrayOf(R.id.id, R.id.nama, R.id.nama2),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsNIl.adapter = listadap
    }

    fun showjenis(jen : String) {
        var sql = ""

        if (jen.equals("1")) {
            sql = "select m.id_jenis as _id , m.nama_jenis , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori and p.nama_kategori ='pengeluaran'"
        }else{
            sql = "select m.id_jenis as _id , m.nama_jenis , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori and p.nama_kategori ='pemasukan'"
        }
        //sql = "select m.id_jenis as _id , m.nama_jenis , p.nama_kategori from jenis m , kategori p where m.id_kategori=p.id_kategori "

        val c: Cursor = db.rawQuery(sql, null)
        listadap = SimpleCursorAdapter(
            thisparent,
            R.layout.isikategori,
            c,
            arrayOf("_id", "nama_jenis", "nama_kategori"),
            intArrayOf(R.id.id, R.id.nama, R.id.nama2),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        v.lsNIl.adapter = listadap
    }

    fun showkat() {
        val c: Cursor = db.rawQuery(
            "select nama_kategori as _id from kategori order by nama_kategori asc",
            null
        )
        spadapjen = SimpleCursorAdapter(
            thisparent, android.R.layout.simple_spinner_item,
            c,
            arrayOf("_id"),
            intArrayOf(android.R.id.text1),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
        )
        spadapjen.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spinnerjen.adapter = spadapjen
        v.spinnerjen.setSelection(0)
    }

    fun insert(nama_jenis: String, id_kategori: Int) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_jenis", nama_jenis)
        cv.put("id_kategori", id_kategori)

        db.insert("jenis", null, cv)
        showjenisAll()

    }
    fun update(nama_jenis: String,id_kategori: Int) {
        var cv: ContentValues = ContentValues()
        cv.put("nama_jenis", nama_jenis)
        cv.put("id_kategori", id_kategori)
        db.update("jenis", cv, "id_jenis='$id_jen'", null)
        showjenisAll()
    }
    fun delete(id_jen : String){
        db.delete("jenis","id_jenis = '$id_jen'",null)
        showjenisAll()

    }


    val indel = DialogInterface.OnClickListener { dialog, which ->
        delete(id_jen)
        v.inputnil.setText("")
    }


    val inbt = DialogInterface.OnClickListener { dialog, which ->

        var sqlkat = "select id_kategori from kategori where nama_kategori ='$nmjen'"
        val b: Cursor = db.rawQuery(sqlkat, null)
        if (b.count > 0) {
            b.moveToFirst()
            insert(
                v.inputnil.text.toString(),
                b.getInt(b.getColumnIndex("id_kategori"))

            )
            v.inputnil.setText("")
        }
    }
    val upbt = DialogInterface.OnClickListener { dialog, which ->

        var sqlkat = "select id_kategori from kategori where nama_kategori ='$nmjen'"
        val b: Cursor = db.rawQuery(sqlkat, null)
        if (b.count > 0) {
            b.moveToFirst()
            update(
                v.inputnil.text.toString(),
                b.getInt(b.getColumnIndex("id_kategori"))

            )
            v.inputnil.setText("")
        }
    }



    val clik = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        id_jen = c.getString(c.getColumnIndex("_id"))
        v.inputnil.setText(c.getString(c.getColumnIndex("nama_jenis")))
        v.spinnerjen.setSelection(c.getInt(c.getColumnIndex("nama_kategori")))

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        spinnerjen.setSelection(0, true)
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val C: Cursor = spadapjen.getItem(position) as Cursor
        nmjen = C.getString(C.getColumnIndex("_id"))
        showjenis(position.toString())
        Toast.makeText(activity!!.baseContext,nmjen, Toast.LENGTH_SHORT).show()

    }
}