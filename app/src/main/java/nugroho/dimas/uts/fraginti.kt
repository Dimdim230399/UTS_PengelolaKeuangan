package nugroho.dimas.uts

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fraginti.*

class fraginti: AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        when(p0.itemId){
            R.id.itemmain->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragmenTransaksi).commit()
                FrameLayout.setBackgroundColor(Color.argb(255,245,255,245))
                FrameLayout.visibility= View.VISIBLE

            }
            R.id.itemsetting->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.FrameLayout,fragmensetting).commit()
                FrameLayout.setBackgroundColor(Color.argb(255,245,255,245))
                FrameLayout.visibility= View.VISIBLE
            }
            R.id.itemabaut-> FrameLayout.visibility = View.GONE

        }
        return true
    }


    lateinit var fragmensetting: fragmensetting
    lateinit var fragmenTransaksi: fragtransaksi
    lateinit var ft : FragmentTransaction
    lateinit var db : SQLiteDatabase
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fraginti)


        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragmenTransaksi = fragtransaksi()
        fragmensetting = fragmensetting()
        db = DBUangku(this).writableDatabase

    }
    fun getdatUang(): SQLiteDatabase {
        return db

    }
}